import XCTest

import RFVideoCaptureTests

var tests = [XCTestCaseEntry]()
tests += RFVideoCaptureTests.allTests()
XCTMain(tests)
