// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
	name: "RFVideoCapture",
	platforms: [
		.iOS(.v14)
	],
	products: [
		.library(
			name: "RFVideoCapture",
			targets: ["RFVideoCapture"]),
	],
	dependencies: [],
	targets: [
		.target(
			name: "RFVideoCapture",
			dependencies: [])
	]
)
