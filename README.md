# RFVideoCapture

This package provides a configurable video capture component based on AVCaptureSession.

The package also provides a custom UIView, [RFVideoCaptureUIView](Sources/RFVideoCapture/RFVideoCaptureUIView.swift), for immediately displaying the video.
Note that `RFVideoCaptureUIView` can not be used in a XIB or Storyboard because it requires an `RFVideoCapture` instance for initialization.

Here is an example of how to embed this `RFVideoCaptureUIView` in your SwiftUI view:

```swift
import SwiftUI
import RFVideoCapture

struct MyVideoView : UIViewRepresentable
{
	func makeUIView(context: Context) -> RFVideoCaptureUIView
	{
		let view = RFVideoCaptureUIView(using: context.coordinator.videoCapture)
		context.coordinator.videoCapture?.startCapturing()
		return view
	}

	func updateUIView(_ uiView: RFVideoCaptureUIView, context: Context)
	{
	}

	func makeCoordinator() -> MyCoordinator
	{
		MyCoordinator()
	}

	class MyCoordinator
	{
		let videoCapture : RFVideoCapture?
		let frameProcessor : MyFrameProcessor

		init()
		{
			frameProcessor = MyFrameProcessor()
			videoCapture = RFVideoCapture(delegate: frameProcessor, settings: [.resFullHD, .frameThrottling])
		}
	}
}
```

where `MyFrameProcessor` implements the `RFVideoCaptureDelegate` protocol as shown below.

```swift
class MyFrameProcessor : RFVideoCaptureDelegate
{
	func registerFilters(for videoCaptureView: RFVideoCaptureUIView)
	{
		// Register your Core Image filters here.
	}

	func process(imageBuffer: CVImageBuffer, for: RFVideoCaptureUIView)
	{
		// Do image processing on the image buffer here.
		
		// You can use the provided utility function to convert the buffer to a CGImage. 
		guard let image = RFVideoCapture.cgImage(from: imageBuffer) else { return }
	}
}
```

