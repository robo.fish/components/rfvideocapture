//
//  RFVideoCapture.swift
//  Core
//
//  Created by Kai Oezer on 22.11.20.
//  Copyright © 2020 Robo.Fish UG. All rights reserved.
//

import CoreImage
import CoreGraphics
import AVFoundation
import UIKit
import Vision
import Combine

public protocol RFVideoCaptureDelegate : AnyObject
{
	func registerFilters(for videoCapture : RFVideoCapture)
	func process(imageBuffer : CVImageBuffer, for videoCapture : RFVideoCapture)
}

/**
This class provides video capturing without creating a view that shows the video.
Use the optional `RFVideoCaptureUIView` for displaying the captured video.
*/
public class RFVideoCapture : NSObject
{
	public struct Settings : OptionSet
	{
		public let rawValue : UInt8
		public init(rawValue : UInt8) { self.rawValue = rawValue }

		public static let resFullHD = Settings(rawValue: 1 << 0)
		public static let	res4K = Settings(rawValue: 1 << 1)
		public static let softwareStabilized = Settings(rawValue: 1 << 3)
		public static let frameThrottling = Settings(rawValue: 1 << 5)
	}

	private let _settings : Settings
	private var _delegate : RFVideoCaptureDelegate?
	private let _captureSession = AVCaptureSession()
	private var _lastCapturedImage : CVPixelBuffer?
	private var _lastCaptureDate : Date?
	private let _stabilizationImageRequestHandler = VNSequenceRequestHandler()
	private var _isCapturingPaused = false
	private var _observer: NSKeyValueObservation?
	private var _cancellableSubscriptions = [AnyCancellable]()

	@objc public dynamic var isCapturing = false

	public var captureSession : AVCaptureSession { _captureSession }

	public init?(delegate : RFVideoCaptureDelegate? = nil, settings : Settings = [])
	{
		_delegate = delegate
		_settings = settings
		super.init()

		delegate?.registerFilters(for: self)

		_captureSession.sessionPreset = settings.contains(.res4K) ? .hd4K3840x2160 : (settings.contains(.resFullHD) ? .hd1920x1080 : .hd1280x720)

		guard let camera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back) else { return nil }
		guard let deviceInput = try? AVCaptureDeviceInput(device: camera) else { return nil }

		guard _captureSession.canAddInput(deviceInput) else { return nil }
		_captureSession.addInput(deviceInput)

		_observer = captureSession.observe(\.isRunning, options: [.new]) { [unowned self] (model, change) in
			DispatchQueue.main.async { [weak self] in self?.isCapturing = change.newValue! }
		}

		_addAppStateObservers()
	}

	public func startCapturing()
	{
		guard !_captureSession.inputs.isEmpty else { return }
		DispatchQueue.global(qos: .userInitiated).async { [weak self] in
			self?._captureSession.startRunning()
			// DispatchQueue.main.async { [weak self] in self?.view._adjustVideoPreviewLayerFrame() }
		}
	}

	public func stopCapturing()
	{
		_captureSession.stopRunning()
	}

	public static func cgImage(from imageBuffer : CVImageBuffer) -> CGImage?
	{
		CVPixelBufferLockBaseAddress(imageBuffer, .readOnly)
		defer { CVPixelBufferUnlockBaseAddress(imageBuffer, .readOnly) }

		let baseAddress = CVPixelBufferGetBaseAddress(imageBuffer)
		let bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer)
		let (width, height) = (CVPixelBufferGetWidth(imageBuffer), CVPixelBufferGetHeight(imageBuffer))
		let bitmapInfo = CGBitmapInfo(rawValue: (CGBitmapInfo.byteOrder32Little.rawValue | CGImageAlphaInfo.premultipliedFirst.rawValue))
		let context = CGContext(data: baseAddress, width: width, height: height, bitsPerComponent: 8, bytesPerRow: bytesPerRow, space: CGColorSpaceCreateDeviceRGB(), bitmapInfo: bitmapInfo.rawValue)
		return context?.makeImage()
	}

	private func _addAppStateObservers()
	{
		let didEnterBgSubscription = NotificationCenter.default.publisher(for: UIApplication.didEnterBackgroundNotification).sink { _ in
			self.stopCapturing()
		}

		let willEnterFgSubscription = NotificationCenter.default.publisher(for: UIApplication.willEnterForegroundNotification).sink { _ in
			self.startCapturing()
		}

		_cancellableSubscriptions.append(contentsOf:[didEnterBgSubscription, willEnterFgSubscription])
	}

}

extension RFVideoCapture : AVCaptureVideoDataOutputSampleBufferDelegate
{
	public func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection)
	{
		if _settings.contains(.frameThrottling)
		{
			let kMinimumScanInterval = 0.1 // seconds
			let now = Date()
			if let lastCaptureDate = _lastCaptureDate {
				if lastCaptureDate.timeIntervalSinceNow > -kMinimumScanInterval { return }
			}
			_lastCaptureDate = now
		}

		guard let frame = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }

		guard !_settings.contains(.softwareStabilized) || _imageStreamHasStabilized(for: frame) else { return }

		_delegate?.process(imageBuffer: frame, for: self)
	}

	/// Uses a simplified version of the technique demonstrated in the WWDC 2018 session
	/// [Vision with Core ML](https://developer.apple.com/videos/play/wwdc2018/717/),
	/// which is based on determining the motion of lines detected in the captured scene.
	private func _imageStreamHasStabilized(for pixelBuffer : CVPixelBuffer) -> Bool
	{
		defer { _lastCapturedImage = pixelBuffer }
		guard let lastImage = _lastCapturedImage else { return false }
		let request = VNTranslationalImageRegistrationRequest(targetedCVPixelBuffer: pixelBuffer)
		try? _stabilizationImageRequestHandler.perform([request], on: lastImage)
		guard let results = request.results as? [VNImageTranslationAlignmentObservation],
			let alignmentTransform = results.first?.alignmentTransform else { return false }
		let distance = alignmentTransform.tx * alignmentTransform.tx + alignmentTransform.ty * alignmentTransform.ty
		let kSomeHeuristicThreshold : CGFloat = 25.0
		return distance < kSomeHeuristicThreshold
	}
}
