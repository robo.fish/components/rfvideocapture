//
//  RFVideoCaptureUIView.swift
//
//  Created by Kai Oezer on 21.11.20.
//  Copyright © 2020 Kai Oezer
//

import UIKit
import AVFoundation

public class RFVideoCaptureUIView : UIView
{
	private let videoCapture : RFVideoCapture?
	private let videoOutput = AVCaptureVideoDataOutput()
	private let videoPreviewLayer = AVCaptureVideoPreviewLayer()

	private var interfaceOrientation : AVCaptureVideoOrientation
	{
		let uiOrientation = self.window?.windowScene?.interfaceOrientation ?? .portrait
		return AVCaptureVideoOrientation(rawValue: uiOrientation.rawValue) ?? .portrait
	}

	public init(using videoCapture : RFVideoCapture?)
	{
		self.videoCapture = videoCapture
		super.init(frame: .zero)
		_initializeCaptureSession()
	}

	override public init(frame: CGRect)
	{
		videoCapture = nil
		super.init(frame: frame)
	}

	required public init?(coder aDecoder: NSCoder)
	{
		videoCapture = nil
		super.init(coder: aDecoder)
	}

	override public func layoutSubviews()
	{
		super.layoutSubviews()
		_adjustVideoPreviewLayerFrame()
	}

	private func _initializeCaptureSession()
	{
		guard let vc = self.videoCapture, vc.captureSession.canAddOutput(videoOutput) else { return }
		vc.captureSession.addOutput(videoOutput)

		videoOutput.setSampleBufferDelegate(vc, queue: DispatchQueue(label: "RFVideoCaptureUIView", qos: .userInteractive, attributes: [], autoreleaseFrequency: .workItem))
		videoOutput.alwaysDiscardsLateVideoFrames = true
		videoOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey: kCVPixelFormatType_32BGRA] as [String : Any]
		videoOutput.connection(with: .video)!.videoOrientation = interfaceOrientation

		videoPreviewLayer.session = vc.captureSession
		videoPreviewLayer.videoGravity = .resizeAspectFill
		layer.insertSublayer(videoPreviewLayer, at: 0)
	}

	private func _adjustVideoPreviewLayerFrame()
	{
		videoOutput.connection(with: .video)?.videoOrientation = interfaceOrientation
		videoPreviewLayer.connection?.videoOrientation = interfaceOrientation
		videoPreviewLayer.frame = bounds
	}
}

